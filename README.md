前提

■環境準備
検証済み環境は以下となる。
検証日時：2018-05-05
 - Windows 10 Home (64bit)
 - Docker Toolbox + Windows10 Home

下記のリンクからtoolboxをダウンロードしてインストールする
* https://docs.docker.com/toolbox/overview/
    - Docker Client
    - Docker Machine
    - Docker Compose (Mac only)
    - Docker Kitematic
    - VirtualBox
    - Git (Windowsの場合 Git for windows)

参考手順：
* https://qiita.com/maemori/items/52b1639fba4b1e68fccd


`Docker Kitematicとは`
Dockerを操作するGUIツール
 - 他の人の作ったコンテナのインストール
 - ホストマシンのブラウザからコンテナ上に立ち上げたサービスを開く
 - 自動でURLが割り当てられる(後述)
 - CLIからの操作も可能

■sugoforce v3について
Cakephp3をベースにした開発環境です。
ビルド構成は以下となります。
    ```
    ├─htdocs
    │  └─sugocake      cakephp3.xのアプリ
    ├─mysql
    │  ├─conf          拡張のための.cnfファイル
    │  └─sql           データベースのインポートファイル
    ├─nginx
    │  └─conf
    │      └─conf.d    default nginxのconfigファイル
    ├─phpfpm
    └─docker-compose.yml  (四つのコンテナー：Mysql5.7 + Adminer + Nginx + phpfpm)
    ```

■利用方法
1. ローカルPCにワークフォルダを作成して、gitでsugoforce v3をクローンします。
```
git-clone https://gitlab.com/sugo-private-group/sugoforce/v3.6.git
```

2. Docker Quickstart Terminalを起動します。
    2.1 正常起動の場合、以下のように表示されます。
    ```
                            ##         .
                  ## ## ##        ==
               ## ## ## ## ##    ===
           /"""""""""""""""""\___/ ===
      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
           \______ o           __/
             \    \         __/
              \____\_______/

    docker is configured to use the default machine with IP 192.168.99.100
    ```

    IPは192.168.99.100であることをメモします。

    2.2 Dockerコンテナーを立ち上げます。
    ```
    cd ワークフォルダ
    docker-compose up -d --build
    ```
    
    2.3 四つのコンテナーがすべてUPされていることを確認します。
    ```
    docker-compose ps

        Name                   Command               State                    Ports
        -------------------------------------------------------------------------------------------------
        sugo_adminer    entrypoint.sh docker-php-e ...   Up      0.0.0.0:8080->8080/tcp
        sugo_cakephp3   docker-php-entrypoint php-fpm    Up      9000/tcp
        sugo_mysql57    docker-entrypoint.sh mysqld      Up      0.0.0.0:3306->3306/tcp
        sugo_nginx      nginx -g daemon off;             Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
    ```

3. 各コンテナーのサービスの正常稼働を確認します。
    - sugo_mysql57
        SKIP、次のsugo_adminerで一緒に確認します。
    - sugo_adminer
        * http://192.168.99.100:8080/
        - ログイン画面に以下の指定して、ログインします。
            - System = MYSQL
            - server = dbserver
            - Username = sugodbuser
            - Password = SUGo.0001
            - Database = sugo_cakephp_db
    - sugo_cakephp3
        phpfpmのコンテナーである
        ```
        docker exec -it sugo_cakephp3 /bin/sh
        cd ./sugocake && php ../composer.phar update 
        cp -f ./config/app.docker.php ./config/app.php
        ```
    - sugo_nginx
        * http://192.168.99.100/

    ※上記の確認はKitematicでも確認できます。